const ROLE = {
  ADMIN: 'admin',
  MODERATOR: 'moderator',
  REGULAR: 'regular',
  ANONIM: 'anonim',
};

const ROLE_LEVELS = {
  [ROLE.ANONIM]: 0,
  [ROLE.REGULAR]: 1,
  [ROLE.MODERATOR]: 2,
  [ROLE.ADMIN]: 3,
};

module.exports = {
  ROLE,
  ROLE_LEVELS,
};
