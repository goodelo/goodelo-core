const { path } = require('ramda');
const { AuthenticationError } = require('apollo-server-lambda');
const { ROLE_LEVELS } = require('../constants/users');

module.exports = {
  privateMethod: (resolver) => async (source, args, context, info) => {
    if (!path(['user', 'username'], context)) {
      throw new AuthenticationError('Only authenticated users allowed');
    }

    return await resolver(source, args, context, info);
  },

  getUserByEvent: (event) => ({
    username: path(
      ['requestContext', 'authorizer', 'claims', 'cognito:username'],
      event
    ),
    email: path(['requestContext', 'authorizer', 'claims', 'email'], event),
    role: path(
      ['requestContext', 'authorizer', 'claims', 'custom:role'],
      event
    ),
  }),

  hasRoleAccess: (role, user) => ROLE_LEVELS[user.role] >= ROLE_LEVELS[role],
};
