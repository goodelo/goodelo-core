const { gql } = require('apollo-server-lambda');

module.exports = gql`
  type Goodelo {
    id: String
    partitionId: String!
    createdAt: Float!
    updatedAt: Float!
    username: String
    identityId: String

    title: String
    description: String
    attachment: String
    lat: Float
    lng: Float
  }

  input GoodeloInput {
    id: String
    identityId: String

    title: String
    description: String
    attachment: String
    lat: Float
    lng: Float
  }

  input CursorInput {
    id: String!
    partitionId: String!
    createdAt: Float!
  }

  type User {
    username: String
    email: String
    role: String
  }

  type UserOutput {
    user: User
    goodelos: [Goodelo!]
  }

  type Cursor {
    id: String!
    partitionId: String!
    createdAt: Float!
  }

  type GoodelosOutput {
    items: [Goodelo!]
    nextPageCursor: Cursor
  }

  type Query {
    goodelosAll: GoodelosOutput!
    goodelos(cursor: CursorInput): GoodelosOutput!
    goodelo(id: String!): Goodelo!
    user(username: String!): UserOutput!
  }

  type Mutation {
    createGoodelo(input: GoodeloInput!): Goodelo!
    updateGoodelo(id: String!, input: GoodeloInput!): Goodelo!
  }
`;
