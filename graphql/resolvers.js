const { privateMethod } = require('./utils/access');

module.exports = {
  Query: {
    goodelos: async (_source, { cursor }, { dataSources }) =>
      dataSources.goodelosAPI.list(cursor),
    goodelosAll: async (_source, _params, { dataSources }) => ({
      items: dataSources.goodelosAPI.listAll(),
    }),
    goodelo: async (_source, { id }, { dataSources }) =>
      dataSources.goodelosAPI.get(id),
    user: async (_source, { username }, { dataSources }) => ({
      user: await dataSources.usersAPI.get(username),
      goodelos: await dataSources.goodelosAPI.listAll({ username }),
    }),
  },

  Mutation: {
    createGoodelo: privateMethod(
      async (_source, { input }, { dataSources, user }) =>
        dataSources.goodelosAPI.create(input, user)
    ),
    updateGoodelo: privateMethod(
      async (_source, { id, input }, { dataSources, user }) =>
        dataSources.goodelosAPI.updateItem({ id, input, user })
    ),
  },
};
