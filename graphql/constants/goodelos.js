const UPDATABLE_FIELDS = ['title', 'description', 'attachment', 'lat', 'lng'];

module.exports = {
  UPDATABLE_FIELDS,
};
