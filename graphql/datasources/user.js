const { DataSource } = require('apollo-datasource');
const AWS = require('aws-sdk');
const { path } = require('ramda');

const { USER_POOL_ID } = require('../constants/config');
const { getUserFromDBResponse } = require('../utils/users');

const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();

class UserAPI extends DataSource {
  constructor(...args) {
    super(...args);
  }

  async get(username) {
    var params = {
      UserPoolId: USER_POOL_ID,
      Filter: `username = "${username}"`,
      Limit: 1,
    };

    const { Users } = await cognitoIdentityServiceProvider
      .listUsers(params)
      .promise();

    return getUserFromDBResponse(Users[0]);
  }
}

module.exports = UserAPI;
