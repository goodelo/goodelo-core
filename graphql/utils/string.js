module.exports = {
  getAlphaNumeric: (str) => str.replace(/[^0-9a-z]/gi, ''),
};
