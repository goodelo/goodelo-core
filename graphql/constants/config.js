const PARTITION_ID = process.env.PARTITION_ID;
const USER_POOL_ID = process.env.USER_POOL_ID;

module.exports = {
  PARTITION_ID,
  USER_POOL_ID,
};
