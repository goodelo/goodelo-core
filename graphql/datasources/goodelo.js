const { DynamoDBDataSource } = require('apollo-datasource-dynamodb');
const { ForbiddenError } = require('apollo-server-lambda');

const uuid = require('uuid');
const {
  pipe,
  fromPairs,
  map,
  pick,
  assoc,
  toPairs,
  isEmpty,
  identity,
} = require('ramda');

const dynamoDb = require('../utils/dynamodb');
const { hasRoleAccess } = require('../utils/access');
const { getAlphaNumeric } = require('../utils/string');
const { ROLE } = require('../constants/users');
const { UPDATABLE_FIELDS } = require('../constants/goodelos');

class GoodeloAPI extends DynamoDBDataSource {
  constructor(config) {
    const tableName = process.env.GOODELOS_TABLE;
    const tableKeySchema = [
      {
        AttributeName: 'partitionId',
        KeyType: 'HASH',
      },
      {
        AttributeName: 'id',
        KeyType: 'RANGE',
      },
    ];
    const ttl = 1; // seconds

    super(tableName, tableKeySchema, config);

    this.tableName = tableName;
    this.partitionId = process.env.PARTITION_ID;
    this.tableKeySchema = tableKeySchema;
    this.ttl = ttl;
  }

  async list(cursor) {
    const params = {
      TableName: this.tableName,
      IndexName: 'id-createdAt-index',
      KeyConditionExpression: 'partitionId = :v1',
      ExpressionAttributeValues: {
        ':v1': this.partitionId,
      },
      ScanIndexForward: false,
      Limit: 10,
      ExclusiveStartKey: cursor || undefined,
    };

    const result = await dynamoDb.query(params).promise();

    return {
      items: result.Items,
      nextPageCursor: result.LastEvaluatedKey,
    };
  }

  async listAll(filters = {}) {
    const filterExpression = Object.keys(filters)
      .map((key) => `#${getAlphaNumeric(key)} = :${getAlphaNumeric(key)}`)
      .join(' AND ');
    const expressionAttributeNames = pipe(
      toPairs,
      map(([key]) => [`#${getAlphaNumeric(key)}`, key]),
      fromPairs
    )(filters);

    const params = pipe(
      isEmpty(expressionAttributeNames)
        ? identity
        : assoc('ExpressionAttributeNames', expressionAttributeNames),
      isEmpty(filterExpression)
        ? identity
        : assoc('FilterExpression', filterExpression)
    )({
      TableName: this.tableName,
      IndexName: 'id-createdAt-index',
      ScanIndexForward: false,
      KeyConditionExpression: 'partitionId = :v1',
      ExpressionAttributeValues: {
        ':v1': this.partitionId,
        ...pipe(
          toPairs,
          map(([key, value]) => [`:${getAlphaNumeric(key)}`, value]),
          fromPairs
        )(filters),
      },
    });

    // @todo: consider scan instead of query
    const result = await dynamoDb.query(params).promise();

    return result.Items;
  }

  async get(id) {
    const params = {
      TableName: this.tableName,
      Key: {
        partitionId: this.partitionId,
        id,
      },
      ConsistentRead: true,
    };
    const result = await this.getItem(params, this.ttl);

    return result;
  }

  async create(input, user) {
    const timestamp = new Date().getTime();

    const params = {
      TableName: this.tableName,
      Item: {
        id: uuid.v1(),
        partitionId: this.partitionId,
        createdAt: timestamp,
        updatedAt: timestamp,
        username: user.username,
        identityId: input.identityId,
        ...pick(UPDATABLE_FIELDS, input),
      },
    };

    const result = await this.put(params.Item, this.ttl);

    return result;
  }

  async updateItem({ id, input, user }) {
    if (!hasRoleAccess(ROLE.MODERATOR, user)) {
      const goodelo = await this.get(id);

      if (user.username !== goodelo.username) {
        throw new ForbiddenError('Only creator can modify');
      }
    }

    const preparedInput = pipe(
      pick(UPDATABLE_FIELDS),
      assoc('updatedAt', new Date().getTime())
    )(input);

    const key = {
      partitionId: this.partitionId,
      id,
    };

    const keysToUpdate = Object.keys(preparedInput);

    const updateExpression =
      `SET ` + keysToUpdate.map((field) => `#${field}=:${field}`).join(',');

    const expressionAttributeNames = pipe(
      map((field) => [`#${field}`, field]),
      fromPairs
    )(keysToUpdate);

    const expressionAttributeValues = pipe(
      map((field) => [`:${field}`, preparedInput[field]]),
      fromPairs
    )(keysToUpdate);

    const result = await this.update(
      key,
      updateExpression,
      expressionAttributeNames,
      expressionAttributeValues,
      this.ttl
    );

    return result;
  }
}

module.exports = GoodeloAPI;
