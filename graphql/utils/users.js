const { path, pipe, propEq, find, pathOr } = require('ramda');

const { ROLE } = require('../constants/users');

module.exports = {
  getUserFromDBResponse: (dbUser) => {
    const attrs = path(['Attributes'], dbUser) || [];

    return {
      username: path(['Username'], dbUser),
      email: pipe(find(propEq('Name', 'email')), path(['Value']))(attrs),
      role: pipe(
        find(propEq('Name', 'custom:role')),
        pathOr(ROLE.REGULAR, ['Value'])
      )(attrs),
    };
  },
};
