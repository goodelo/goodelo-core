const { ApolloServer } = require('apollo-server-lambda');

const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const GoodeloAPI = require('./datasources/goodelo');
const UserAPI = require('./datasources/user');
const { getUserByEvent } = require('./utils/access');

const dataSources = () => ({
  goodelosAPI: new GoodeloAPI(),
  usersAPI: new UserAPI(),
});

const context = ({ event, context }) => ({
  headers: event.headers,
  functionName: context.functionName,
  event,
  context,
  user: getUserByEvent(event),
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources,
  context,
  playground: process.env.STAGE === 'staging',
});

const graphqlHandler = server.createHandler({
  cors: {
    origin: process.env.ALLOWED_ORIGIN,
  },
});

module.exports.handler = graphqlHandler;
